# NasaApod

A new Flutter project.

## Getting Started

Initialize project by installing dependencies:

```sh
flutter pub get
```

Initialize environment config with:

```sh
flutter pub run environment_config:generate
```

Run app with (requires emulator or connected device):

```sh
flutter run
```

## Assets

The app icon can be found in `assets/icon`. There are several variants and at time of writing there are all WIP. The svg needs to be exported as png. Afterwards change the path in `pubspec.yaml` for the package [flutter_launch_icons](https://pub.dev/packages/flutter_launcher_icons):

```yml
flutter_icons:
  android: "launcher_icon"
  ios: true
  image_path: "assets/icon/icon_1024x1024.png"
```

Generate all necessary icons with the mentioned package by

```sh
flutter pub run flutter_launcher_icons:main
```

Currently used icon is `icon_v2_thick.svg`. The `icon_v2.svg` can be used for bigger screens.

## ToDo

* [ ] provider better config
* [ ] finish app ;-)