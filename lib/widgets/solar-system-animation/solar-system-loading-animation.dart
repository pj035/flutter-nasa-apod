// NOTE: painting code idea from
// https://github.com/daniilbug/solar_system_clock/blob/master/solar_system_clock/lib/planets_painter.dart

import 'package:flutter/material.dart';

import 'package:NasaApod/widgets/solar-system-animation/constants.dart';
import 'package:NasaApod/widgets/solar-system-animation/solar-system-painter.dart';
import 'package:NasaApod/widgets/solar-system-animation/celestial.model.dart';

class SolarSystemLoadingAnimation extends StatefulWidget {
  final double _radius;

  SolarSystemLoadingAnimation({double radius = DEFAULT_WIDTH / 2})
      : _radius = radius < MIN_ANIMATION_RADIUS ? MIN_ANIMATION_RADIUS : radius;

  @override
  _SoloarSystemLoadingAnimationState createState() =>
      _SoloarSystemLoadingAnimationState();
}

class _SoloarSystemLoadingAnimationState
    extends State<SolarSystemLoadingAnimation> with TickerProviderStateMixin {
  Animation<double> _rotationAnimation;
  AnimationController _controller;

  Map<ECelestial, Celestial> _solarSystem;

  @override
  void initState() {
    super.initState();

    _solarSystem = initializeCelestialMap(true, 2);

    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: ANIMATION_TIME),
    );

    // NOTE: output values until the "longest" (time-wise) rotation is complete,
    // then repeast
    Tween<double> rotationTween = Tween(begin: 0, end: ANIMATION_TIME * 1.0);

    _rotationAnimation = rotationTween.animate(_controller)
      ..addListener(() {
        setState(() {
          _solarSystem.forEach((key, celestial) {
            final orbitRatio = LONGEST_ORBIT_TIME / celestial.orbitTime;
            celestial.currentOrbitAngle += orbitRatio;
          });
        });
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          setState(() {});
          _controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          _controller.forward();
        }
      });

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _rotationAnimation,
      builder: (context, snapshot) {
        return CustomPaint(
          size: Size(widget._radius * 2, widget._radius * 2),
          painter: SolarSystemPainter(
              solarSystem: _solarSystem, radius: widget._radius),
        );
      },
    );
  }
}
