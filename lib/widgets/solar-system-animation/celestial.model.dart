import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const MIN_SUN_RADIUS = 12.5;

final random = Random();

class Celestial {
  Celestial(this.color, this.orbitTime,
      {this.radius = 2, this.currentOrbitAngle = 0});

  final double radius;
  final Color color;

  /// Orbit time around the sun.
  final double orbitTime;

  /// Inidicates initial offset to the orbit time; used for animation purpose.
  double currentOrbitAngle;
}

/// Identifier for the solar system's celestials.
enum ECelestial {
  Sun,
  Mercury,
  Venus,
  Earth,
  Mars,
  Jupiter,
  Saturn,
  Uranus,
  Neptun,
}

// NOTE: orbit time is in days
final defaultCelestials = {
  ECelestial.Sun: Celestial(Colors.yellow.shade700, 0, radius: MIN_SUN_RADIUS),
  ECelestial.Mercury: Celestial(Colors.grey, 88),
  ECelestial.Venus: Celestial(Colors.blueAccent, 225),
  ECelestial.Earth: Celestial(Colors.blue, 365),
  ECelestial.Mars: Celestial(Colors.deepOrangeAccent, 687),
  ECelestial.Jupiter: Celestial(Colors.amberAccent, 4329),
  ECelestial.Saturn: Celestial(Colors.amber, 10751),
  ECelestial.Uranus: Celestial(Colors.teal, 30664),
  ECelestial.Neptun: Celestial(Colors.blueGrey, 60148),
};

/// Initialize a (randomized) celestial map.
/// [randomEntryAngleOffset] set to true will create each celestial with a random
/// start angle.
/// Orbit time is calculated as the logarithm to base [orbitTimeBase].
Map<ECelestial, Celestial> initializeCelestialMap(
    bool randomEntryAngleOffset, double orbitTimeBase) {
  final cpy = Map<ECelestial, Celestial>();
  defaultCelestials.forEach((key, celestial) {
    final celestialCpy = Celestial(
        celestial.color, logBase(celestial.orbitTime, orbitTimeBase),
        radius: celestial.radius, currentOrbitAngle: 0);
    cpy.putIfAbsent(key, () => celestialCpy);
  });

  if (!randomEntryAngleOffset) {
    return cpy;
  }

  cpy.forEach((key, celestial) {
    final celestialCpy = Celestial(celestial.color, celestial.orbitTime,
        radius: celestial.radius, currentOrbitAngle: random.nextDouble() * 360);
    cpy.update(key, (c) => celestialCpy);
  });

  return cpy;
}

/// https://github.com/dart-lang/sdk/issues/38519
double logBase(num x, num base) => x == 0 ? 0 : log(x) / log(base);
