import 'dart:math';

import 'package:flutter/material.dart';

import 'package:NasaApod/widgets/solar-system-animation/celestial.model.dart';

const MIN_SPACE_RATIO = 8.0;

class SolarSystemPainter extends CustomPainter {
  /// Radius of the whole system
  final double _radius;
  final _paint = Paint()..style = PaintingStyle.fill;
  final Map<ECelestial, Celestial> solarSystem;

  // TODO: allow dynamic size for the whole drawing
  SolarSystemPainter({this.solarSystem, @required double radius})
      : _radius = radius;

  static double _degreeToRadian(double angleInDegree) {
    return angleInDegree * pi / 180;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;
    final center = Offset(width / 2, height / 2);

    final sun = solarSystem[ECelestial.Sun];
    final maxSpace = _radius - sun.radius;
    var spaceRatio = maxSpace / (solarSystem.length - 1);
    if (spaceRatio < MIN_SPACE_RATIO) {
      spaceRatio = MIN_SPACE_RATIO;
    }
    var counter = 0;

    solarSystem.forEach((celestialId, celestial) {
      if (celestialId == ECelestial.Sun) {
        // sun doesn't need any animation
        canvas.drawCircle(
            center, celestial.radius, _paint..color = celestial.color);
        counter++;
        return;
      }

      final distanceToCenter = (spaceRatio * counter) + sun.radius;

      // Skip if element is outside of our draw radius
      if (distanceToCenter > _radius) {
        return;
      }

      final drawCenter = _pointsForAngle(center,
          _degreeToRadian(celestial.currentOrbitAngle), distanceToCenter);
      canvas.drawCircle(
          drawCenter, celestial.radius, _paint..color = celestial.color);

      counter++;
    });
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }

  Offset _pointsForAngle(Offset center, double angle, double distance) {
    final x = center.dx;
    final y = center.dy;
    final dx = (distance) * cos(angle);
    final dy = (distance) * sin(angle);
    return Offset(x + dx, y + dy);
  }
}
