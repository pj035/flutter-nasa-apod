const DEFAULT_WIDTH = 180.0;

/// Orbit of Neptun = log(60148, 2)
const LONGEST_ORBIT_TIME = 15.88;

/// deprecated: this has no effect
/// time for a full rotation of the "slowest" celestial
const ANIMATION_TIME = 5;

const MIN_ANIMATION_RADIUS = 50.0;
