import 'package:NasaApod/shared/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:NasaApod/models/apod.model.dart';
import 'package:NasaApod/pages/apod-detail.page.dart';
import 'package:NasaApod/shared/constants.dart';
import 'package:NasaApod/widgets/solar-system-animation/solar-system-loading-animation.dart';

class ApodListItem extends StatelessWidget {
  final APODItem _item;
  final int _index;
  final bool _isLastItem;

  ApodListItem(this._item, this._index, this._isLastItem);

  @override
  Widget build(BuildContext context) {
    final heroTag = '${HERO_APOD_LIST_TO_DETAIL_PAGE}_$_index';

    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ApodDetailPage(
            apodItem: _item,
            heroTag: heroTag,
          ),
        ),
      ),
      child: Hero(
        tag: heroTag,
        child: _buildImageCart(),
      ),
    );
  }

  Widget _buildCartTitle() {
    return Container(
      padding: EdgeInsets.all(8),
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Container(
            child: Text(
              apodDateFormatter.format(_item.date),
              style: TextStyle(fontSize: 8),
            ),
            alignment: Alignment.topRight,
          ),
          Container(
            // margin: EdgeInsets.only(top: 10.0),
            child: Text(_item.title),
            alignment: Alignment.center,
          ),
        ],
      ),
    );
  }

  Widget _buildApodImage(String url) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, url) => Center(
          child: SolarSystemLoadingAnimation(radius: 75),
        ),
        fit: BoxFit.fitWidth,
        fadeInDuration: Duration(milliseconds: 500),
      ),
    );
  }

  Widget _buildMedia() {
    if (_item.isImage) {
      return _buildApodImage(_item.url);
    } else {
      if (_item.url.contains('youtube')) {
        final url = getYoutubeThumbnailUrl(_item.url);
        return Stack(children: [
          _buildApodImage(url),
          Container(
            padding: EdgeInsets.all(4),
            alignment: Alignment.topRight,
            child: Icon(
              Icons.play_circle_outline_rounded,
              size: 20,
            ),
          ),
        ]);
      } else {
        return SizedBox(
          width: double.infinity,
          height: 100,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(height: 15),
              Text('Embedded URL. Click for details.'),
              Icon(Icons.laptop_chromebook),
              SizedBox(height: 15),
            ],
          ),
        );
      }
    }
  }

  Widget _buildImageCart() {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 5,
      margin: EdgeInsets.fromLTRB(10, 10, 10, _isLastItem ? 10 : 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildCartTitle(),
          _buildMedia(),
        ],
      ),
    );
  }
}
