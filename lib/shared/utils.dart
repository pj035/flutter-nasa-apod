import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final apodDateFormatter = DateFormat('dd.MM.yyyy');

bool isSameDay(DateTime a, DateTime b) => a.difference(b).inDays == 0;

bool isInDateRange(DateTime target, DateTimeRange range) {
  return (range.start.isBefore(target) ||
          range.start.isAtSameMomentAs(target)) &&
      (range.end.isAfter(target) || range.end.isAtSameMomentAs(target));
}

// TODO: throw / handle ids that can not be extracted
String getYoutubeThumbnailUrl(String youtubeUrl) {
  final videoId = youtubeUrl
      .replaceAll('https://www.youtube.com/embed/', '')
      .replaceAll('?rel=0', '');

  return 'https://img.youtube.com/vi/$videoId/hqdefault.jpg';
}
