import 'dart:collection';

import 'package:NasaApod/shared/constants.dart';
import 'package:NasaApod/shared/utils.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

import 'package:NasaApod/shared/nasa-apod-api.dart';
import 'package:NasaApod/models/apod.model.dart';

class ApodProvider extends ChangeNotifier {
  /// The list of available APoD items.
  final List<APODItem> _apodItems = [];
  // TODO: is map more performant on the read operations than a list?
  /// A map of items . The value indicates if API returned an item.
  final Map<DateTime, bool> _alreadyTriggeredDates = Map();
  bool _isLoading = false;
  DateTime _oldestItemTimestamp;

  ApodProvider() {
    // pull first batch of data once during init process
    this.fetchApodItems();
  }

  Future<void> _loadApodData(DateTime startDate, DateTime endDate) async {
    _isLoading = true;
    notifyListeners();
    try {
      final items = await fetchApodData(startDate, endDate);
      items.forEach((item) {
        _alreadyTriggeredDates.putIfAbsent(item.date, () => true);
        bool exists = _apodItems.indexOf(item) != -1;
        if (!exists) {
          _apodItems.add(item);
        }
      });
      // NOTE: insertion sort should be fine, because apoditems are almost
      // sorted (https://www.toptal.com/developers/sorting-algorithms/nearly-sorted-initial-order)
      insertionSort(_apodItems);
      _isLoading = false;
      notifyListeners();
      return items;
    } catch (e) {
      _isLoading = false;
      notifyListeners();
      return [];
    }
  }

  /// Fetches the next batch of apod items.
  Future<void> fetchApodItems() async {
    // NOTE/impl: fetch first "page"
    if (_oldestItemTimestamp == null) {
      final today = DateTime.now();
      _oldestItemTimestamp =
          today.subtract(new Duration(days: DEFAULT_ITEM_PAGE_SIZE));
      await this._loadApodData(_oldestItemTimestamp, today);
      return;
    }

    // TODO: fetch new data once we reach almost the end of the list
    // TODO: impl error handling when reaching last page
    if (_isLoading ||
        (_apodItems.isNotEmpty &&
            _apodItems.last.date.isAtSameMomentAs(_oldestItemTimestamp))) {
      throw ('E_ALREADY_LOADING');
    }

    // NOTE/impl: fetch next "page"
    final endDate = _oldestItemTimestamp.subtract(new Duration(days: 1));
    _oldestItemTimestamp =
        endDate.subtract(new Duration(days: DEFAULT_ITEM_PAGE_SIZE));
    await this._loadApodData(_oldestItemTimestamp, endDate);
  }

  /// Loads an APOD item of a specific date.
  /// throws `E_API_ALREADY_LOADED` in case date has already been loaded in the past;
  /// throws `E_API_NO_DATA` in case date has been loaded, but API returned
  /// empty data.
  Future<void> fetchApodItemAtDate(DateTime date) async {
    final comparisonDate = DateTime(date.year, date.month, date.day);
    final apiTriggered = _alreadyTriggeredDates.containsKey(comparisonDate);
    if (apiTriggered == true) {
      if (_alreadyTriggeredDates[comparisonDate] == true) {
        throw ('E_API_ALREADY_LOADED');
      } else {
        throw ('E_API_NO_DATA');
      }
    }

    return _loadApodData(date, date);
  }

  Future<void> fetchApodItemsByDateRange(DateTimeRange range) {
    if (range == null) {
      throw ('E_NULL_INPUT');
    }
    // TODO: avoid loading items multiple times
    return this._loadApodData(range.start, range.end);
  }

  /// Indicates whether apod items are still under loading or not.
  bool get isLoading => _isLoading;

  /// Returns the list of available apod items.
  /// The items are sorted from most recent timestamp to oldest timestamp.
  UnmodifiableListView<APODItem> get apodItems =>
      UnmodifiableListView(_apodItems);

  /// Returns a filtered list of apod items, that contains the item at
  /// the specified `date`.
  UnmodifiableListView<APODItem> getApodItemsFilteredByDate(DateTime date) {
    if (date == null) {
      return UnmodifiableListView(_apodItems);
    }

    return UnmodifiableListView(
        _apodItems.where((element) => isSameDay(element.date, date)));
  }

  /// Returns a filtered list of apod items, that contains the item inside
  /// the specified `range`.
  UnmodifiableListView<APODItem> getApodItemsFilteredByDateRange(
      DateTimeRange range) {
    if (range == null) {
      return UnmodifiableListView(_apodItems);
    }

    return UnmodifiableListView(
        _apodItems.where((element) => isInDateRange(element.date, range)));
  }
}
