const DEFAULT_ITEM_PAGE_SIZE = 5;

const NEXT_PAGE_TRIGGER_INDEX_DIFFERENCE = 5;

const API_NASA_APOD = 'https://api.nasa.gov/planetary/apod';

// Hero Tags
const HERO_APOD_LIST_TO_DETAIL_PAGE = 'apod:list-to-detail';
const HERO_PHOTO_VIEW_DETAIL_PAGE = 'apod:detail:photo_view';
