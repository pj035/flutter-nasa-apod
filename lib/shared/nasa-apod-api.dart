// API source: https://github.com/nasa/apod-api

import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:NasaApod/shared/constants.dart';
import 'package:NasaApod/models/apod.model.dart';
import 'package:NasaApod/shared/env_config.dart';

/// Requests NASA APOD API to fetch the data specified in the date range.
/// If no [endDate] is specified, the device's local date will be used.
/// [startDate] MUST be after 1995-06-16, because before that no APOD is available.
Future<List<APODItem>> fetchApodData(DateTime startDate,
    [DateTime endDate]) async {
  if (startDate.isBefore(new DateTime(1995, 6, 16))) {
    throw ('startDate is before 1995-06-16');
  }

  var queryEndDate = null;
  if (endDate != null) {
    if (endDate.isBefore(startDate)) {
      throw ('endDate must be after startDate');
    }

    queryEndDate = endDate.toIso8601String().split('T').first;
  }

  final queryStartDate = startDate.toIso8601String().split('T').first;
  // TODO: use real API key from env (or so)
  var url =
      '${API_NASA_APOD}?api_key=${EnvConfig.NASA_API_KEY}&start_date=${queryStartDate}';
  if (queryEndDate != null) {
    url += '&end_date=${queryEndDate}';
  }

  final response = await http.get(url);

  if (response.statusCode != 200) {
    // TODO: error handling
    return [];
  }

  final body = jsonDecode(response.body);

  // NOTE: API returns a simple object if only one day has been requested
  if (!(body is List)) {
    return [APODItem.fromJSON(body)];
  } else if (body is List) {
    final items = body.map((e) => APODItem.fromJSON(e)).toList();
    items.sort((a, b) => -1 * a.date.compareTo(b.date));
    return items;
  } else {
    // TODO: error handling - can this case actually happen?
    return [];
  }
}
