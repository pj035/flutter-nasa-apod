import 'package:NasaApod/shared/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:NasaApod/models/apod.model.dart';
import 'package:NasaApod/shared/utils.dart';
import 'package:NasaApod/widgets/solar-system-animation/solar-system-loading-animation.dart';
import 'package:link_text/link_text.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ApodDetailPage extends StatefulWidget {
  final APODItem _apod;
  final String _heroTag;

  ApodDetailPage({APODItem apodItem, String heroTag})
      : _apod = apodItem,
        _heroTag = heroTag;
  @override
  _ApodDetailPageState createState() => _ApodDetailPageState();
}

class _ApodDetailPageState extends State<ApodDetailPage> {
  /// Simple helper
  APODItem get _apod => widget._apod;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_apod.title),
      ),
      body: ListView(children: [
        _buildMedia(),
        _buildApodDescription(),
      ]),
    );
  }

  Widget _buildApodImage() {
    return GestureDetector(
      onScaleStart: (_) => _showPhotoView(),
      onTap: () => _showPhotoView(),
      child: Hero(
        tag: widget._heroTag,
        child: CachedNetworkImage(
          imageUrl: _apod.bestQualityUrl,
          placeholder: (context, url) => Center(
            child: SolarSystemLoadingAnimation(radius: 75),
          ),
          fit: BoxFit.fitWidth,
          fadeInDuration: Duration(milliseconds: 500),
        ),
      ),
    );
  }

  Widget _buildMedia() {
    if (_apod.isImage) {
      return _buildApodImage();
    } else {
      if (_apod.url.contains('youtube')) {
        return SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.5,
          child: WebView(
            initialUrl: _apod.url,
            javascriptMode: JavascriptMode.unrestricted,
            debuggingEnabled: false,
          ),
        );
      } else {
        // likely embedded nasa
        return SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.2,
          child: Container(
            padding: EdgeInsets.all(16),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Embedded URL. Click the URL to open a webpage.'),
                  SizedBox(height: 16),
                  LinkText(
                    _apod.url,
                    textAlign: TextAlign.center,
                    onLinkTap: (url) => launch(url),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    }
  }

  Widget _buildApodDescription() {
    final date = apodDateFormatter.format(_apod.date);
    final copyRight = _apod.copyright != null ? '${_apod.copyright}' : '';

    return Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: [
            Text(_apod.title),
            SizedBox(
              height: 20,
            ),
            Text(_apod.description),
            SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.only(left: 0),
              child: copyRight.isNotEmpty
                  ? Text('${copyRight} - ${date}')
                  : Text('${date}'),
            )
          ],
        ));
  }

  void _showPhotoView() {
    final imagePage = CachedNetworkImage(
      imageUrl: _apod.bestQualityUrl,
      imageBuilder: (context, provider) {
        return PhotoView(
          imageProvider: provider,
          filterQuality: FilterQuality.high,
          minScale: PhotoViewComputedScale.contained,
          heroAttributes: const PhotoViewHeroAttributes(
            tag: HERO_PHOTO_VIEW_DETAIL_PAGE,
            transitionOnUserGestures: true,
          ),
        );
      },
    );
    Navigator.push(context, MaterialPageRoute(builder: (context) => imagePage));
  }
}
