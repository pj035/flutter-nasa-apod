import 'package:NasaApod/shared/nasa-apod.provider.dart';
import 'package:NasaApod/widgets/apod-list-item.dart';
import 'package:NasaApod/widgets/solar-system-animation/solar-system-loading-animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class ApodList extends StatefulWidget {
  @override
  _ApodListState createState() => _ApodListState();
}

class _ApodListState extends State<ApodList> {
  ScrollController _listScrollCtrl;
  DateTimeRange _dateFilter;

  @override
  void initState() {
    super.initState();
    _listScrollCtrl = ScrollController();
    _listScrollCtrl.addListener(_listScrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Astronomy Picture of the Day'),
        actions: [
          IconButton(
              icon: Icon(
                _dateFilter == null ? Icons.event : Icons.event_available,
              ),
              onPressed: _filterByDate)
        ],
      ),
      body: _buildApodList(),
    );
  }

  Widget _buildApodList() {
    return Consumer<ApodProvider>(
      builder: (context, apodProvider, child) {
        final items = apodProvider.getApodItemsFilteredByDateRange(_dateFilter);
        return ListView.builder(
          controller: _listScrollCtrl,
          itemCount: items.length + 1,
          itemBuilder: (context, index) {
            if (items.length > index) {
              final isLastItem = items.length - 1 == index;
              final item = items[index];
              return ApodListItem(
                item,
                index,
                isLastItem,
              );
            }

            // NOTE: reached end
            if (apodProvider.isLoading) {
              return Container(
                margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
                alignment: Alignment.center,
                child: Center(
                  child: SolarSystemLoadingAnimation(),
                ),
              );
            }

            // TODO
            return Container();
          },
        );
      },
    );
  }

  void _listScrollListener() {
    // NOTE/impl: don't trigger loading if items are filtered
    if (_dateFilter != null) {
      return;
    }
    final provider = Provider.of<ApodProvider>(context, listen: false);
    if (_listScrollCtrl.position.atEdge &&
        _listScrollCtrl.position.pixels != 0) {
      // TODO: error handling required?
      provider.fetchApodItems().catchError((e) {});
    }
  }

  void _filterByDate() async {
    try {
      final range = await showDateRangePicker(
        context: context,
        initialDateRange: _dateFilter,
        firstDate: new DateTime(1995, 6, 17),
        lastDate: DateTime.now(),
        helpText: 'Show Pictures of a specific Date Range.',
        confirmText: 'Filter by Range',
        saveText: 'Show!',
        cancelText: 'Show all Entries',
      );
      setState(() {
        _dateFilter = range;
      });

      if (range == null) {
        return;
      }

      final provider = Provider.of<ApodProvider>(context, listen: false);
      await provider.fetchApodItemsByDateRange(range);
    } catch (error) {
      // TODO: show message to user that no data is available for that day
      print(error);
    }
  }
}
