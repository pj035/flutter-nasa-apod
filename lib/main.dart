import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart' as loadLocale;
import 'package:provider/provider.dart';

import 'package:NasaApod/shared/nasa-apod.provider.dart';
import 'package:NasaApod/pages/apod-list.page.dart';

void main() async {
  await loadLocale.initializeDateFormatting('de_DE', null);
  runApp(
    // TODO/impl: is this the right place to register our provider?
    ChangeNotifierProvider(
      create: (context) => ApodProvider(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Astronomy Picture of the Day',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: MaterialColor(0xffffffff, const {
          50: const Color(0xffFAFAFA),
          100: const Color(0xffF5F5F5),
          200: const Color(0xffEEEEEE),
          300: const Color(0xffE0E0E0),
          400: const Color(0xffBDBDBD),
          500: const Color(0xff9E9E9E),
          600: const Color(0xff757575),
          700: const Color(0xff616161),
          800: const Color(0xff424242),
          900: const Color(0xff212121)
        }),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: Scaffold(
        body: ApodList(),
      ),
    );
  }
}
