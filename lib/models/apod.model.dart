import 'package:flutter/widgets.dart';

class APODItem extends Comparable {
  ///  The title of the image.
  final String title;

  /// Date of image. Included in response because of default values.
  final DateTime date;

  /// The supplied text explanation of the image.
  final String description;

  /// The URL of the APOD image or video of the day.
  final String url;

  /// The URL for any high-resolution image for that day. Returned regardless of 'hd' param setting but will be omitted in the response IF it does not exist originally at APOD.
  String _hdUrl;

  /// The type of media (data) returned. May either be 'image' or 'video' depending on content.
  String _mediaType;

  /// The URL of thumbnail of the video
  String _thumbnailUrl;

  /// The name of the copyright holder.
  String _copyright;

  APODItem({
    @required this.title,
    @required this.date,
    @required this.description,
    @required this.url,
    String hdUrl,
    String mediaType,
    String thumbnailUrl,
    String copyright,
  })  : _hdUrl = hdUrl,
        _mediaType = mediaType,
        _thumbnailUrl = thumbnailUrl,
        _copyright = copyright;

  factory APODItem.fromJSON(Map<String, dynamic> json) {
    final date = DateTime.parse(json['date']);
    final truncatedDate = DateTime(date.year, date.month, date.day);
    return APODItem(
      title: json['title'],
      date: truncatedDate,
      description: json['explanation'],
      url: json['url'],
      hdUrl: json['hd_url'],
      mediaType: json['media_type'],
      copyright: json['copyright'],
      thumbnailUrl: json['thumbnail_url'],
    );
  }

  /// The URL for any high-resolution image for that day. Returned regardless of 'hd' param setting but will be omitted in the response IF it does not exist originally at APOD.
  String get hdUrl => this._hdUrl;

  /// The type of media (data) returned. May either be 'image' or 'video' depending on content.
  String get mediaType => this._mediaType;

  /// The URL of thumbnail of the video
  String get thumbnailUrl => this._thumbnailUrl;

  /// The name of the copyright holder.
  String get copyright => this._copyright;

  bool get isVideo => this.mediaType == 'video';

  bool get isImage => this.mediaType == 'image';

  /// Returns the resource URL ordered by best (available) quality.
  String get bestQualityUrl => this._hdUrl != null ? this._hdUrl : this.url;

  @override
  String toString() {
    return this.title + ' ' + this.date.toString();
  }

  // https://stackoverflow.com/questions/29567322/how-does-a-set-determine-that-two-objects-are-equal-in-dart
  @override
  bool operator ==(other) {
    return date.isAtSameMomentAs(other.date);
  }

  int _hashcode;
  @override
  int get hashCode {
    if (_hashcode == null) {
      _hashcode = date.hashCode;
    }
    return _hashcode;
  }

  /// Compares APOD items by date in a descending order, i.e. from
  /// youngest (most recent) to oldest.
  @override
  int compareTo(other) {
    if (this == other) {
      return 0;
    }

    return date.isBefore(other.date) ? 1 : -1;
  }
}
