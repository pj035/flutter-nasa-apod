import 'package:flutter/material.dart';
import 'package:test/test.dart';
import 'package:intl/date_symbol_data_local.dart' as loadLocale;

import 'package:NasaApod/shared/utils.dart';

void main() async {
  await loadLocale.initializeDateFormatting('de_DE', null);
  group('date format', () {
    test('should convert date to German dd.mm.yyyy format', () {
      final date = DateTime(2020, 5, 13);
      final result = apodDateFormatter.format(date);
      expect(result, '13.05.2020');
    });
  });

  group('isSameDay', () {
    test('it should return true if dates are on the same day', () {
      final a = DateTime(2000, 10, 10, 15, 30, 30);
      final b = DateTime(2000, 10, 10, 9, 30, 30);
      expect(isSameDay(a, b), true);
    });

    test('it should return true if dates are on different days', () {
      final a = DateTime(2000, 10, 10, 15, 30, 30);
      final b = DateTime(1990, 10, 10, 9, 30, 30);
      expect(isSameDay(a, b), false);
    });
  });

  group('isInDateRange', () {
    test('it returns true if date is inside range', () {
      final range = DateTimeRange(
          start: DateTime(2020, 05, 06), end: DateTime(2020, 05, 10));
      final targets = [
        DateTime(2020, 05, 6),
        DateTime(2020, 05, 7),
        DateTime(2020, 05, 10),
      ];

      targets.forEach((element) => expect(isInDateRange(element, range), true));
    });

    test('it returns false if date is outside range', () {
      final target1 = DateTime(2019);
      final target2 = DateTime(2021);
      final range =
          DateTimeRange(start: DateTime(2020, 05), end: DateTime(2020, 06));

      expect(isInDateRange(target1, range), false);
      expect(isInDateRange(target2, range), false);
    });
  });

  group('getYoutubeThumbnailUrl', () {
    test('returns a thumbnail url', () {
      final source = 'https://www.youtube.com/embed/WJua8eXLX9o?rel=0';
      final result = getYoutubeThumbnailUrl(source);
      final expectation =
          'https://img.youtube.com/vi/WJua8eXLX9o/hqdefault.jpg';
      expect(result, expectation);
    });
  });
}
