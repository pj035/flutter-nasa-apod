import 'dart:io';

import 'package:NasaApod/shared/constants.dart';
import 'package:NasaApod/shared/env_config.dart';
import 'package:NasaApod/shared/nasa-apod.provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nock/nock.dart';

void main() async {
  // HELPER
  final apiSingleResponse =
      new File('test/shared/fixtures/nasa-apod/nasa-api-single.response.json')
          .readAsStringSync();
  final apiSingleResponse2 = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-single-2007.response.json')
      .readAsStringSync();
  final apiListResponse =
      new File('test/shared/fixtures/nasa-apod/nasa-api-list.response.json')
          .readAsStringSync();
  final apiListUnsortedResponse = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-list-unsorted.response.json')
      .readAsStringSync();
  final apiListPage1 = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-list-page1.response.json')
      .readAsStringSync();
  final apiListPage2 = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-list-page2.response.json')
      .readAsStringSync();
  final apiListPage3 = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-list-page3.response.json')
      .readAsStringSync();

  final dateStr = (date) => date.toIso8601String().split('T').first;

  /// Returns a data mock matching any query parameters, returning 200
  /// and the provided string as response.
  final getGeneralInterceptor = (String resBody) => nock(API_NASA_APOD).get('')
    // Match any query
    ..query((Map<String, List<String>> params) => true)
    ..reply(200, resBody);

  final getSpecificDateInterceptor =
      (String resBody, DateTime start, DateTime end) =>
          nock(API_NASA_APOD).get('')
            ..query({
              'api_key': EnvConfig.NASA_API_KEY,
              'start_date': dateStr(start),
              'end_date': dateStr(end),
            })
            ..reply(200, resBody);

  final getProvider = () async {
    final ApodProvider provider = ApodProvider();
    // artifical delay to await fetching inside the constructor
    await Future.delayed(Duration(seconds: 1));
    return provider;
  };

  final isDone = (Interceptor interceptor) => expect(interceptor.isDone, true);

  final verifySortedDescending = (List items) {
    for (int i = 0; i < items.length - 1; i++) {
      final current = items[i];
      final next = items[i + 1];
      expect(current.date.isAfter(next.date), true);
    }
  };

  // HELPER_END

  setUpAll(() {
    nock.init();
  });

  setUp(() {
    nock.cleanAll();
  });

  group('fetchApodItems', () {
    test('it should call submit an API request during init process', () async {
      final interceptor = getGeneralInterceptor(apiSingleResponse);

      final provider = await getProvider();

      expect(provider.apodItems.length, 1);
      isDone(interceptor);
    });

    test('it should insert sorted items from most recent to oldest', () async {
      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      expect(provider.apodItems.length, 6);
      verifySortedDescending(provider.apodItems);
      isDone(interceptor);
    });

    test('it should insert unsorted items from most recent to oldest',
        () async {
      final interceptor = getGeneralInterceptor(apiListUnsortedResponse);

      final provider = await getProvider();

      expect(provider.apodItems.length, 6);
      verifySortedDescending(provider.apodItems);
      isDone(interceptor);
    });

    test('it should not add duplicate items', () async {
      final interceptors = [
        getGeneralInterceptor(apiListUnsortedResponse),
        getGeneralInterceptor(apiListUnsortedResponse)
      ];

      final provider = await getProvider();
      await provider.fetchApodItems();

      expect(provider.apodItems.length, 6);
      verifySortedDescending(provider.apodItems);
      interceptors.forEach(isDone);
    });
  });

  group('it filters apod items by date', () {
    test('it returns a filtered list with the date\'s item', () async {
      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      final filterDate = DateTime(2021, 3, 3);
      final item = provider.getApodItemsFilteredByDate(filterDate).first;
      expect(provider.getApodItemsFilteredByDate(filterDate).length, 1);
      expect(item.title, 'Stars over an Erupting Volcano');
      isDone(interceptor);
    });

    test('it returns an empty list if date can not be found', () async {
      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      final filterDate = DateTime(2002, 3, 3);
      expect(provider.getApodItemsFilteredByDate(filterDate).length, 0);
      isDone(interceptor);
    });

    test('it returns the original list if date is null', () async {
      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      expect(provider.getApodItemsFilteredByDate(null).length, 6);
      expect(provider.getApodItemsFilteredByDate(null), provider.apodItems);
      isDone(interceptor);
    });
  });

  group('fetchApodItemAtDate', () {
    test('throws an error if date has already been loaded', () async {
      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      final date = DateTime(2021, 3, 3);
      expect(() => provider.fetchApodItemAtDate(date),
          throwsA(equals('E_API_ALREADY_LOADED')));

      isDone(interceptor);
    });

    test('it loads data of the specified day', () async {
      final date = DateTime(2007, 9, 26);
      final interceptor2 =
          getSpecificDateInterceptor(apiSingleResponse2, date, date);

      final interceptor = getGeneralInterceptor(apiListResponse);

      final provider = await getProvider();

      await provider.fetchApodItemAtDate(date);

      final items = provider.getApodItemsFilteredByDate(date);
      expect(items.length, 1);
      expect(items.first.title, 'Saguaro Moon');
      expect(provider.apodItems.length, 7);

      isDone(interceptor);
      isDone(interceptor2);
    });
  });

  group('fetchApodItemsByDateRange', () {
    test('it throws an error if date range is null', () async {
      final ApodProvider provider = ApodProvider();
      expect(() => provider.fetchApodItemsByDateRange(null),
          throwsA('E_NULL_INPUT'));
    });

    test('it triggers API for the specified date range', () async {
      // constructor init
      final interceptor = getGeneralInterceptor(apiSingleResponse);

      final start = DateTime(2007, 9, 25);
      final end = DateTime(2007, 9, 27);

      final interceptor2 =
          getSpecificDateInterceptor(apiSingleResponse2, start, end);

      final provider = await getProvider();

      await provider
          .fetchApodItemsByDateRange(DateTimeRange(start: start, end: end));

      expect(provider.apodItems.length, 2);
      isDone(interceptor);
      isDone(interceptor2);
    });

    test('it inserts all items in the correct order', () async {
      final interceptorList = [
        getSpecificDateInterceptor(
            apiListPage1, DateTime(2017, 07, 04), DateTime(2017, 07, 10)),
        getSpecificDateInterceptor(
            apiListPage2, DateTime(2017, 07, 11), DateTime(2017, 07, 17)),
        getSpecificDateInterceptor(
            apiListPage3, DateTime(2017, 07, 18), DateTime(2017, 07, 24)),
      ];

      final provider = await getProvider();
      await provider.fetchApodItemsByDateRange(DateTimeRange(
          start: DateTime(2017, 07, 11), end: DateTime(2017, 07, 17)));
      await provider.fetchApodItemsByDateRange(DateTimeRange(
          start: DateTime(2017, 07, 18), end: DateTime(2017, 07, 24)));
      await provider.fetchApodItemsByDateRange(DateTimeRange(
          start: DateTime(2017, 07, 04), end: DateTime(2017, 07, 10)));

      expect(provider.apodItems.length, 21);
      verifySortedDescending(provider.apodItems);

      interceptorList.forEach(isDone);
    });
  });
}
