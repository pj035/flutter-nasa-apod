import 'dart:convert';
import 'dart:io';

import 'package:NasaApod/models/apod.model.dart';
import 'package:NasaApod/shared/env_config.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nock/nock.dart';

import 'package:NasaApod/shared/constants.dart';
import 'package:NasaApod/shared/nasa-apod-api.dart';

void main() async {
  final apiSingleResponse =
      new File('test/shared/fixtures/nasa-apod/nasa-api-single.response.json')
          .readAsStringSync();
  final apiListResponse =
      new File('test/shared/fixtures/nasa-apod/nasa-api-list.response.json')
          .readAsStringSync();

  setUpAll(() {
    nock.init();
  });

  setUp(() {
    nock.cleanAll();
  });

  group('fetchApodData', () {
    test('it throws an error if startDate is before APOD supported time frame',
        () {
      final errorDate = DateTime(1995, 6, 15);
      expect(() => fetchApodData(errorDate, DateTime.now()),
          throwsA(equals('startDate is before 1995-06-16')));
    });

    test('it throws an error if startdate is after enddate', () {
      final startDate = DateTime(2000);
      final endDate = DateTime(1999);
      expect(() => fetchApodData(startDate, endDate),
          throwsA(equals('endDate must be after startDate')));
    });

    // TODO: refactor once the http is handled properly
    test('it returns an empty list if response has not http code 200',
        () async {
      final startDate = DateTime(1998, 10, 10);

      final interceptor = nock(API_NASA_APOD).get('')
        ..query({
          'api_key': EnvConfig.NASA_API_KEY,
          'start_date': '${startDate.year}-${startDate.month}-${startDate.day}',
        })
        ..reply(400, '');

      expect(await fetchApodData(startDate), []);
      expect(interceptor.isDone, true);
    });

    test('it should return a list even if a single apod item is returned',
        () async {
      final startDate = DateTime(1998, 10, 10);

      final interceptor = nock(API_NASA_APOD).get('')
        ..query({
          'api_key': EnvConfig.NASA_API_KEY,
          'start_date': '${startDate.year}-${startDate.month}-${startDate.day}',
        })
        ..reply(200, apiSingleResponse);

      final result = await fetchApodData(startDate);
      final expectedResult = APODItem.fromJSON(jsonDecode(apiSingleResponse));

      expect(result is List, true);
      expect(result.first.title, expectedResult.title);
      expect(interceptor.isDone, true);
    });

    test(
        'it should return a list sorted by descending date (most recent to oldest)',
        () async {
      final startDate = DateTime(1998, 10, 10);

      final interceptor = nock(API_NASA_APOD).get('')
        ..query({
          'api_key': EnvConfig.NASA_API_KEY,
          'start_date': '${startDate.year}-${startDate.month}-${startDate.day}',
        })
        ..reply(200, apiListResponse);

      final result = await fetchApodData(startDate);

      expect(result is List, true);
      expect(result.first.date.isAfter(result.last.date), true);
      expect(interceptor.isDone, true);
    });
  });
}
