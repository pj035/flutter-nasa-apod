import 'dart:convert';
import 'dart:io';

import 'package:NasaApod/models/apod.model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() async {
  final apiSingleResponse =
      new File('test/shared/fixtures/nasa-apod/nasa-api-single.response.json')
          .readAsStringSync();
  final apiSingleResponse2 = new File(
          'test/shared/fixtures/nasa-apod/nasa-api-single-2007.response.json')
      .readAsStringSync();
  group('APODItem', () {
    test('two items with same date should return true on == operator', () {
      final item1 = APODItem.fromJSON(jsonDecode(apiSingleResponse));
      final item2 = APODItem.fromJSON(jsonDecode(apiSingleResponse));

      expect(item1 == item2, true);
    });

    test('it compares two items according to their date in descending order',
        () {
      final younger = APODItem.fromJSON(jsonDecode(apiSingleResponse));
      final older = APODItem.fromJSON(jsonDecode(apiSingleResponse2)[0]);
      expect(younger.compareTo(older), -1);
      expect(older.compareTo(younger), 1);
      expect(younger.compareTo(younger), 0);
      expect(older.compareTo(older), 0);
    });
  });
}
